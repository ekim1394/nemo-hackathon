from fastapi import FastAPI, File, UploadFile
from fastapi.responses import FileResponse
from fastapi.staticfiles import StaticFiles
import shutil
from translate import Translator
from pydantic import BaseModel

# import model

app = FastAPI()

app.mount("/static", StaticFiles(directory="http/static"), name="static")

class Text(BaseModel):
    text: str

@app.get('/')
async def home():
    return FileResponse('http/index.html')


@app.post('/transcribe')
async def create_upload_file(file: UploadFile = File(...)):
    # Add logic to do stuff with file here.
    print(file.filename)
    transcript = ""
    # Save file
    with open('audio.wav', 'wb') as f:
        f.write(file.file.read())

    # Transcribe file
    transcript = model.inference('audio.wav')
    return {"transcript": transcript}

@app.post('/translate')
async def translate_text(text: Text):
    translator= Translator(to_lang="Spanish")
    translation = translator.translate(text.text)
    return {"translate": translation}