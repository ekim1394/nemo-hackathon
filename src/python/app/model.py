import nemo.collections.asr as nemo_asr
try:
    model = nemo_asr.models.EncDecCTCModel.from_pretrained(model_name="QuartzNet15x5Base-En")
except Exception as e:
    print(e)
    print("Unable to load model")

def inference(file):
    """Takes in .wav file and transcribes it to english. Requires GPU.

    Parameters
    ----------
    file : .wav file
        .wav file of english audio
    """
    transcript = model.transcribe(paths2audio_files=[file])
    return transcript