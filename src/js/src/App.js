import React, {Component} from "react";
import '@fortawesome/fontawesome-free'

import 'bootstrap/dist/css/bootstrap.min.css';
import AudioAnalyser from "react-audio-analyser"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faPlay, faStop, faPause, faMicrophone} from '@fortawesome/free-solid-svg-icons'
import 'axios'
import axios from "axios";

export default class demo extends Component {
  constructor(props) {
      super(props)
      this.state = {
          status: "",
          transcript: "",
          translate: ""
      }
  }

  componentDidMount() {
  }

  controlAudio(status) {
      this.setState({
          status
      })
  }

  translateTranscript() {
    var self = this;
    axios.post('/translate', {"text": self.state.transcript})
        .then(res => {
            self.setState({translate: res.data.translate})
        })
        .catch(err => {
            console.log(err)
        })
  }

  render() {
      const {status, transcript, audioSrc, translate} = this.state;
      const audioProps = {
          audioType: "audio/wav",
          // audioOptions: {sampleRate: 16000},
          status,
          audioSrc,
          timeslice: 1000, // timeslice（https://developer.mozilla.org/en-US/docs/Web/API/MediaRecorder/start#Parameters）
          startCallback: (e) => {
              console.log("succ start", e)
          },
          pauseCallback: (e) => {
              console.log("succ pause", e)
          },
          stopCallback: (e) => {
            // Add logic to send call to API
            console.log(e)
            let fileUrl  = window.URL.createObjectURL(e)
            console.log(fileUrl)
            let formData = new FormData();
            formData.append('file', e, "audio.wav")
            var self = this;
            axios.post('/transcribe', formData, {headers: {'Content-Type': 'audio/wav'}})
                .then(function(response){
                  console.log(response)
                  self.setState({transcript: response.data.transcript[0]})
                })
                .catch(function(error){
                  console.log("Can't reach backend API.")
                  console.log(error)
                })
            this.setState({
                audioSrc: window.URL.createObjectURL(e)
            })
            console.log("succ stop", e)
          },
          onRecordCallback: (e) => {
              console.log("recording", e)
          },
          errorCallback: (err) => {
              console.log("error", err)
          }
      }

      

      return (
          <div className="align-self-center">
          <h1 className="align-self-center">Aetna Digital Hackathon</h1>
          <h2>BI & Analytics</h2>
          <p>Prototype application to showcase capabilities of <a href="https://github.com/NVIDIA/NeMo">NVIDIA NeMo</a></p>
              <AudioAnalyser {...audioProps}>
                  <div className="btn-box">
                      {status !== "recording" &&
                      <FontAwesomeIcon icon={faMicrophone} className="iconfont" onClick={() => this.controlAudio("recording")}/>}
                      {status === "recording" &&
                      <FontAwesomeIcon icon={faPause} className="iconfont"
                         onClick={() => this.controlAudio("paused")}/>}
                      <FontAwesomeIcon icon={faStop} className="iconfont"
                         onClick={() => this.controlAudio("inactive")}/>
                  </div>
              </AudioAnalyser>

              {transcript !== "" &&
              <div className="row mb-3">
              <div className="col-md-3"/>
              <div className="card col-md-6 bg-primary">
              <div className="card-body">{transcript}</div>
              <div className="col-md-3"/>
              </div>
              </div>
              }
              
              <button className="btn btn-primary" onClick={() => this.translateTranscript()}>Translate!</button>
              {translate !== "" &&
              <div className="row mb-3">
              <div className="col-md-3"/>
              <div className="card col-md-6 bg-success">
              <div className="card-body">{translate}</div>
              <div className="col-md-3"/>
              </div>
              </div>
              }
          </div>
      );
  }
}